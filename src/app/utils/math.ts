export const map = (num: number, inMin: number, inMax: number, outMin: number, outMax: number) => {
  return (num - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
};

export const clamp = (num: number, min: number, max: number) => {
  return Math.min(Math.max(num, min), max);
};

export const mapAndClamp = (num: number, inMin: number, inMax: number, outMin: number, outMax: number): number => {
  return clamp(map(num, inMin, inMax, outMin, outMax), outMin, outMax);
};
