import {moveItemInArray} from './arrays';



export interface Table {
  headers: TableHeader[];
  rows: TableRow[];
}


export interface TableHeader {
  id: number;
  label: string;
}


export interface TableRow {
  cells: TableCell[];
}


export interface TableCell {
  id: number;
  content: number|string;
}


export enum SortDirection {
  ASC, DESC
}


export const oppositeDirection = (sortDirection: SortDirection): SortDirection => {
  if (sortDirection === SortDirection.DESC) {
    return SortDirection.ASC;
  } else {
    return SortDirection.DESC;
  }
};


export const cloneTable = (table: Table): Table => {
  return {
    ...table,
    headers: [...table.headers],
    rows: table.rows.map((row) => ({
      ...row,
      cells: [...row.cells]
    }))
  }
};


export const swapTableColumns = (table: Table, fromIndex: number, toIndex: number): void => {
  moveItemInArray<TableHeader>(table.headers, fromIndex, toIndex);
  table.rows.forEach((row) => {
    moveItemInArray(row.cells, fromIndex, toIndex);
  })
};


export const sortTableByColumnId = (table: Table, columnId: number, direction: SortDirection): void => {
  const columnIndex = table.headers.findIndex((header) => header.id === columnId);

  if (columnIndex === undefined) {
    return;
  }

  table.rows.sort((a, b) => {
    const cellA = a.cells[columnIndex];
    const cellB = b.cells[columnIndex];

    const isBigger = direction === SortDirection.ASC ? 1 : -1;
    const isSmaller = direction === SortDirection.ASC ? -1 : 1;

    return cellA.content > cellB.content ? isBigger : cellA.content < cellB.content ? isSmaller : 0;
  });
};


export const sortTableColumns = (table: Table, columnIds: number[]): void => {
  table.headers = columnIds.map(columnId => table.headers.find((header) => header.id === columnId)) as TableHeader[];
  table.rows = table.rows.map((row) => ({
    ...row,
    cells: columnIds.map(columnId => row.cells.find((cell) => cell.id === columnId)) as TableCell[]
  }));
};
