type Address = string;


export interface TokenBalance {
  symbol: string;
  name: string;
  address: Address|null;
  decimals: number;
  balance: number;
  priceUsd: number;
  balanceUsd: number;
}


export interface AccountBalance {
  address: Address;
  native: TokenBalance;
  tokens: TokenBalance[];
  assets: TokenBalance[];
}
