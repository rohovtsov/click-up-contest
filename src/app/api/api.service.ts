import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {AccountBalance} from './interfaces/account-balance';
import {parseAccountBalance} from './parsers/account-balance';
import {retryWithDelay} from 'rxjs-boost/lib/operators';



const RETRY_DELAY = 1000;
const RETRY_COUNT = 5;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }


  public getAccountBalance(address: string): Observable<AccountBalance> {
    const apiKey = environment.ethplorer_api_key;
    const url = `https://api.ethplorer.io/getAddressInfo/${address}?apiKey=${apiKey}`;

    return this.http.get(url).pipe(
      map((res: any): AccountBalance => {
        return parseAccountBalance(res);
      }),
      retryWithDelay(RETRY_DELAY, RETRY_COUNT)
    );
  }
}
