import {AccountBalance, TokenBalance} from '../interfaces/account-balance';



export const parseAccountBalance = (res: any): AccountBalance => {
  if (res.error) {
    throw new Error(JSON.stringify(res.error));
  }

  const ethDecimals = 18;
  const ethBalance = res.ETH.balance;
  const ethPrice = res.ETH.price.rate;
  const eth: TokenBalance = {
    name: 'Ethereum',
    symbol: 'ETH',
    address: null,
    decimals: ethDecimals,
    balance: ethBalance,
    balanceUsd: ethBalance * ethPrice,
    priceUsd: ethPrice,
  };

  const tokens: TokenBalance[] = (res.tokens ?? []).map((token: any) => {
    const priceObject = token.tokenInfo.price;
    const decimals = token.tokenInfo.decimals ?? 18;
    const priceUsd = (priceObject && priceObject.currency === 'USD' ? priceObject.rate : null) ?? 0;
    const balance = token.balance / 10 ** decimals ?? 0;

    return {
      address: token.tokenInfo.address,
      symbol: token.tokenInfo.symbol,
      name: token.tokenInfo.name,
      decimals,
      balance,
      priceUsd,
      balanceUsd: balance * priceUsd,
    };
  });

  return {
    address: res.address,
    native: eth,
    tokens: tokens,
    assets: [...tokens, eth],
  };
};
