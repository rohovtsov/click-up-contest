import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {SortDirection, Table} from '../../utils/table';
import {TableController} from './table-controller';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit, OnChanges {
  @Input() table: Table;
  displayedTable: Table;
  controller: TableController;

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.controller = new TableController(this.table);
    this.displayTable();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('table' in changes && this.controller) {
      this.controller.setInitialTable(this.table);
      this.displayTable();
    }
  }

  onDrop(event: CdkDragDrop<string, any>) {
    this.controller.swapColumns(event.previousIndex, event.currentIndex);
    this.displayTable();
  }

  onSortToggleClick(id: number): void {
    this.controller.setNextSort(id);
    this.displayTable();
  }

  private displayTable(): void {
    this.displayedTable = this.controller.createTable();
    this.changeDetectorRef.detectChanges();
  }

  hasSortToggle(id: number): boolean {
    const { sort } = this.controller;
    return !!sort && sort.columnId === id;
  }

  isSortToggleAsc(id: number): boolean {
    if (id !== this.controller.sort?.columnId) {
      return true;
    }

    return this.controller.sort?.direction === SortDirection.ASC;
  }
}
