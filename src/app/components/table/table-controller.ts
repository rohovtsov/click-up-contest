import {cloneTable, SortDirection, sortTableByColumnId, sortTableColumns, Table} from '../../utils/table';
import {moveItemInArray} from '../../utils/arrays';


export interface TableControllerSort {
  direction: SortDirection;
  columnId: number;
}

export class TableController {
  public sort: TableControllerSort|null = null;
  public columnsSort: number[];

  constructor(
    private initialTable: Table
  ) {
    this.columnsSort = this.initialTable.headers.map((header => header.id));
  }

  public setInitialTable(table: Table): void {
    this.initialTable = table;
  }

  public setNextSort(columnId: number): TableControllerSort|null {
    const prevColumnId = this.sort?.columnId ?? null;
    let prevDirection = prevColumnId === columnId ? (this.sort?.direction ?? null) : null;

    let newDirection =
      prevDirection === null ? SortDirection.ASC :
      prevDirection === SortDirection.ASC ? SortDirection.DESC :
      prevDirection === SortDirection.DESC ? null : null;

    if (newDirection !== null) {
      this.sort = {
        columnId,
        direction: newDirection
      }
    } else {
      this.sort = null;
    }

    return this.sort;
  }

  public swapColumns(fromIndex: number, toIndex: number) {
    moveItemInArray(this.columnsSort, fromIndex, toIndex);
  }

  public createTable(): Table {
    const newTable = cloneTable(this.initialTable);

    sortTableColumns(newTable, this.columnsSort);

    if (this.sort) {
      sortTableByColumnId(newTable, this.sort.columnId, this.sort.direction);
    }

    return newTable;
  }
}
