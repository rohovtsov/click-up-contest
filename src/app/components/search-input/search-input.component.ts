import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';



@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {
  @Output() searchChange = new EventEmitter<string>();
  searchControl: FormControl;

  constructor( ) { }

  ngOnInit(): void {
    this.searchControl = new FormControl('');
    this.searchControl.valueChanges.subscribe(this.searchChange);
  }
}
