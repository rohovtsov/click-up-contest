import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-account-input',
  templateUrl: './account-input.component.html',
  styleUrls: ['./account-input.component.scss']
})
export class AccountInputComponent implements OnInit, OnChanges {
  @Input() address: string;
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      address: new FormControl(this.address, [
        Validators.required,
        Validators.minLength(42),
        Validators.maxLength(42),
        (control) => {
          return /(?:0x)?[a-fA-F0-9]{40}/.test(control.value) ? null : {
            regex: true
          }
        }
      ])
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('address' in changes && this.formGroup) {
      this.formGroup.setValue({
        address: this.address
      })
    }
  }

  onSubmit(): void {
    if (!this.formGroup.valid) {
      return;
    }

    this.router.navigate(['/balance', this.formGroup.value.address])
  }

  isDisabled(): boolean {
    return !this.formGroup.valid;
  }
}
