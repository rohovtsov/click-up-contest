import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageAccountBalanceComponent} from './pages/page-account-balance/page-account-balance.component';
import {environment} from '../environments/environment';



const routes: Routes = [
  { path: 'balance/:address', component: PageAccountBalanceComponent },
  { path: '**', redirectTo: `balance/${environment.default_balance_address}` }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
