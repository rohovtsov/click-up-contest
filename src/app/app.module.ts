import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './components/table/table.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PageAccountBalanceComponent } from './pages/page-account-balance/page-account-balance.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AccountInputComponent} from './components/account-input/account-input.component';
import {SearchInputComponent} from './components/search-input/search-input.component';



@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    PageAccountBalanceComponent,
    PageAccountBalanceComponent,
    AccountInputComponent,
    SearchInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DragDropModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
