import {TokenBalance} from '../../api/interfaces/account-balance';



export const searchTokens = (
  tokens: TokenBalance[],
  search: string,
): TokenBalance[] => {
  if (search === '') {
    return [...tokens];
  }

  return tokens.filter((token) => {
    const keywords = [
      (token.symbol ?? '').trim().toLowerCase(),
      (token.name ?? '').trim().toLowerCase(),
      (token.balance.toString() ?? '').trim().toLowerCase(),
      (token.balanceUsd.toString() ?? '').trim().toLowerCase(),
      (token.priceUsd.toString() ?? '').trim().toLowerCase(),
    ];

    return keywords.some((keyword) => keyword.includes(search));
  });
};

