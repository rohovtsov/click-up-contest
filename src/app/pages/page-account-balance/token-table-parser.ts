import {Table} from '../../utils/table';
import {TokenBalance} from '../../api/interfaces/account-balance';



export const parseTokenTable = (
  tokens: TokenBalance[],
  headers: string[],
  map: (token: TokenBalance) => (number|string)[]
): Table => {

  return {
    headers: headers.map((header, index) => ({
      id: index,
      label: header
    })),
    rows: tokens.map((token) => {
      const cells: (number|string)[] = map(token);

      return {
        cells: cells.map((cell, index) => ({
          id: index,
          content: cell
        }))
      }
    })
  };
};

