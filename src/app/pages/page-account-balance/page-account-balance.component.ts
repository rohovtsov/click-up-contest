import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {ApiService} from '../../api/api.service';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap, tap} from 'rxjs/operators';
import {Table} from '../../utils/table';
import {parseTokenTable} from './token-table-parser';
import {searchTokens} from './tokens-search';



@Component({
  selector: 'app-page-account-balance',
  templateUrl: './page-account-balance.component.html',
  styleUrls: ['./page-account-balance.component.scss']
})
export class PageAccountBalanceComponent implements OnInit {
  table$: Observable<Table>;
  address$: Observable<string>;
  search$: BehaviorSubject<string>;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) {
    this.search$ = new BehaviorSubject('');
  }

  ngOnInit(): void {
    const address$ = this.route.params.pipe(
      map((params: any): string => params.address.trim())
    );

    const tokens$ = address$.pipe(
      switchMap((address) => this.api.getAccountBalance(address).pipe(
        map(balance => balance.assets.slice(0, 500))
      ))
    );

    const uniqueSearch$ = this.search$.pipe(
      map(search => search.trim().toLowerCase()),
      debounceTime(400),
      startWith(''),
      distinctUntilChanged()
    );

    const table$ = combineLatest([tokens$, uniqueSearch$]).pipe(
      map(([tokens, search]): Table => parseTokenTable(
        searchTokens(tokens, search),
        ['Symbol', 'Name', 'Balance', 'Price (Usd)', 'Balance (Usd)'],
        (token) => [token.symbol, token.name, token.balance, token.priceUsd, token.balanceUsd]
      ))
    );

    this.table$ = table$;
    this.address$ = address$;
  }

  onSearch(query: string) {
    this.search$.next(query);
  }
}
